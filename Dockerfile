# FROM alpine:3.6
FROM debian:stretch
# RUN echo -e "http://dl-3.alpinelinux.org/alpine/edge/main\nhttp://dl-3.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
# 	apk update && \
# 	apk add texlive && \
# 	mkdir /export && \
#     texconfig rehash
RUN apt-get update && \
    apt-get --no-install-recommends install -y texlive-base texlive-bibtex-extra texlive-extra-utils texlive-generic-recommended texlive-fonts-recommended texlive-font-utils texlive-latex-base texlive-latex-recommended texlive-latex-extra texlive-math-extra texlive-pictures texlive-pstricks texlive-science perl-tk purifyeps chktex latexmk dvipng dvidvi fragmaster lacheck latexdiff libfile-which-perl dot2tex tipa latex-xcolor latex-beamer prosper pgf lmodern
#     apt-get install -y texlive
RUN apt-get install -y --no-install-recommends texlive-lang-german && \
    apt-get clean && \
    rm -rf /var/lib/apt/* /var/lib/dpkg/* /etc/apt/* /etc/dpkg/* /var/cache/apt/*

ADD startup.sh /startup.sh
ENTRYPOINT ["/startup.sh"]
