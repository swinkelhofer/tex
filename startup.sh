#!/bin/bash

echo $WD
PROG=$1
shift 
cd $WD
exec $(basename $PROG) "$@"
