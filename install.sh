#!/bin/bash

cp texlaunch /usr/local/bin/texlaunch
for i in "bibtex8" "pdflatex" "makeglossaries" "makeindex"
do
    ln -fs /usr/local/bin/texlaunch /usr/local/bin/$i
done
